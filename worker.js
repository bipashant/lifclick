var device = "other";
var userAgent = navigator.userAgent.toLowerCase();

if (/ipad|iphone|ipod/i.test(userAgent)){
    device = "iphone";
}

if (/android/i.test(userAgent)) {
    device = "android";
}

if (/blackberry/i.test(userAgent)) {
    device = "isBlackBerry";
}

if (/windows phone/i.test(userAgent)) {
    device = "isWindowsPhone";
}

var browser = "other";

if (/safari/i.test(userAgent)){
    browser = "safari";
}

if (/chrome/i.test(userAgent)){
    browser = "chrome";
}

if (/firefox/i.test(userAgent)){
    browser = "firefox";
}

var browser = function() {
    // Return cached result if avalible, else get result then cache it.
    if (browser.prototype._cachedResult)
        return browser.prototype._cachedResult;

    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]"
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;

    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;

    return browser.prototype._cachedResult =
        isOpera ? 'Opera' :
            isFirefox ? 'Firefox' :
                isSafari ? 'Safari' :
                    isChrome ? 'Chrome' :
                        isIE ? 'IE' :
                            isEdge ? 'Edge' :
                                isBlink ? 'Blink' :
                                    "Don't know";
};



console.log("userAgent " + "is " + userAgent);
console.log(browser() + " in " + device);

